class Session
  attr_reader :talks, :length

  def initialize(length)
    @talks = []
    if length.is_a?(Integer)
      @length = length
    else
      raise "Length is not a integer"
    end
  end

  def add_talk(talk)
    if time_available?(talk)
      @length = @length - talk.length
      @talks << talk
    end
  end

  def time_available?(talk)
    talk.length <= @length
  end
end
