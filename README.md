Usage
=====

1. Setup Ruby, Bundler.
2. Run `bundle install`.
3. Put your file in the db folder and change the name of the file in application.rb.
4. Run `ruby application.rb`.

You get an output of all the tracks and the talks allotted to them.

Running Tests
=============

Run `bundle exec rspec` to run all the tests.
