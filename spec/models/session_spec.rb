require 'spec_helper'

describe Session do

  it 'has the ability to store multiple talks' do
    session = Session.new(180)
    expect(session.talks).to be_kind_of(Array)
  end

  it 'can add talks to itself' do
    session = Session.new(180)
    talk1 = Talk.new('Common Ruby Errors lightning')
    talk2 = Talk.new('Writing Fast Tests Against Enterprise Rails 60min')
    session.add_talk(talk1)
    session.add_talk(talk2)
    expect(session.talks.count).to eq 2
  end

  it 'adds talks only if there is time left' do
    session = Session.new(55)
    talk1 = Talk.new('Common Ruby Errors lightning')
    talk2 = Talk.new('Writing Fast Tests Against Enterprise Rails 60min')
    session.add_talk(talk1)
    session.add_talk(talk2)
    expect(session.talks.count).to eq 1
  end

  it 'acknowledges if there is time available for a talk or not' do
    session = Session.new(55)
    talk1 = Talk.new('Writing Fast Tests Against Enterprise Rails 60min')
    talk2 = Talk.new('Writing Fast Tests Against Enterprise Rails 55min')
    expect(session.time_available?(talk1)).to eq false
    expect(session.time_available?(talk2)).to eq true
  end

  it 'raises an error when the length is not a integer' do
    expect { Session.new('55') }.to raise_error
  end
end
