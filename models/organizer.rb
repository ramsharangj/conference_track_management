class Organizer
  attr_reader :talks, :sessions

  def initialize(talks, sessions)
    @talks = talks
    @sessions = load_sessions(sessions)
  end

  def organize
    @sessions.each do |session|
      @talks.each do |talk|
        next if talk.processed? || session.talks.include?(talk)
        if session.time_available?(talk)
          session.add_talk(talk)
          talk.processed
        end
      end
    end
    sessions
  end

  private
  def load_sessions(sessions)
    sessions.map { |session| Session.new(session) }
  end
end
