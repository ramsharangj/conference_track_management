class Talk
  attr_reader :length, :title
  LIGHTNING_LENGTH = 5

  def initialize(talk_title)
    @title = talk_title
    @length = parse_length(talk_title)
    @processed = false
  end

  def processed
    @processed = true
  end

  def processed?
    @processed
  end

  private
  def parse_length(talk_title)
    talk_title = talk_title.split
    if talk_title.last == "lightning"
      LIGHTNING_LENGTH
    elsif match = talk_title.last.match(/(\d+)min/)
      match[1].to_i
    else
      raise "Invalid length format #{talk_title.last}"
    end
  end
end
