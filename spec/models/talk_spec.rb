require 'spec_helper'

describe Talk do
  it 'formats the talks into objects from a list' do
    talk = Talk.new('Writing Fast Tests Against Enterprise Rails 60min')
    expect(talk.length).to eq 60
    expect(talk.title).to eq 'Writing Fast Tests Against Enterprise Rails 60min'
  end

  it 'sets the length to 5 when there is lightning in the title' do
    talk = Talk.new('Common Ruby Errors lightning')
    expect(talk.length).to eq 5
    expect(talk.title).to eq 'Common Ruby Errors lightning'
  end

  it 'marks the talk as processed when processed method is triggered' do
    talk = Talk.new('Common Ruby Errors 45min')
    talk.processed
    expect(talk.processed?).to be true
  end

  it 'raises an error when there is an error when the length is in wrong format' do
    expect { Talk.new('Writing Fast Tests Against Enterprise Rails 60mi') }.to raise_error(RuntimeError)
  end
end
