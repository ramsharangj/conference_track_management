require_relative './models/talk.rb'
require_relative './models/session.rb'
require_relative './models/organizer.rb'

require 'date'

file_contents = File.open(File.expand_path('./db/input.txt'))
talks = file_contents.map do |content|
  content = content.gsub(/\n/, '')
  Talk.new(content)
end

session_lengths = [180, 180, 240, 240]

organizer = Organizer.new(talks, session_lengths)

timetable = organizer.organize

timetable.each_with_index do |session, index|
  #Define tracks
  if (index + 1).odd?
    puts "Track 1"
  else
    puts "Track 2"
  end

  #Define sessions
  if index < 2
    puts "Morning Session"
    time = DateTime.new(2015, 10, 27, 9)
  else
    puts "Evening Session"
    time = DateTime.new(2015, 10, 27, 13)
  end

  #Print timetable for session
  session.talks.each do |talk|
    puts "#{time.strftime('%H:%M')} #{talk.title}"
    time = time + Rational(talk.length.to_i, 1440)
  end

  #Define lunch breaks
  if index < 2
    time = DateTime.new(2015, 10, 27, 12)
    puts "#{time.strftime('%H:%M')} Lunch"
  else
    time = DateTime.new(2015, 10, 27, 17)
    puts "#{time.strftime('%H:%M')} Networking event"
  end
end
