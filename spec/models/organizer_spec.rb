require 'spec_helper'

describe Organizer do
  let(:parsed_file_contents) { parse_contents(file_contents) }
  let(:file_contents) { File.open(file_path) }
  let(:file_path) { File.expand_path("../../fixtures/input.txt", __FILE__) }

  it 'organizes the talks into sessions' do
    talks = parsed_file_contents.map { |content| Talk.new(content) }
    sessions = [105, 120]
    organizer = Organizer.new(talks, sessions)
    expect(organizer.organize.count).to eq 2
    expect(organizer.organize.first.talks).to eq([talks[0], talks[1]])
    expect(organizer.organize.last.talks).to eq([talks[2], talks[3], talks[4]])
  end

  private
  def parse_contents(file_contents)
    file_contents.map { |content| content.gsub(/\n/, '') }
  end
end
